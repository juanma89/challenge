# backEnd
## dependencias
    "cors": "^2.8.5",
    "dotenv": "^16.3.1",
    "express": "^4.18.2",
    "mongodb": "^5.7.0",
    "mongoose": "^7.3.1",
    "nodemon": "^2.0.22"
    -----------
## archivo connect.js : 
Se utiliza MongoClient, clase de MongoDB para Node.js que permite establecer y administrar conexiones con un servidor de MongoDB.
Establece una conexion a una base de datos MongoDB utilizando el modulo MongoClient. La función toma una URL de conexión como argumento y utiliza dicha URL para conectarse a la base de datos. dentro del try se obtiene una referencia a la colección "Clientes" en la base de datos "DB_Clientes".
 Luego, se realiza una consulta para encontrar un documento en la colección "Clientes" donde el campo "Nombre" sea igual a "Dario".
 Aca presento errores

 ## app.js : 
  Archivo principal. Configura un servidor Express y el enrutamiento de solicitudes HTTP, se importa la función connectDB desde el archivo connect.js, que establece una conexión a la base de datos MongoDB utilizando la URL proporcionada en la variable de entorno MONGODB_URL.
  ( app.use ) define una ruta : /api/user , que se gestionará mediante el modulo ( userRoutes ) por ultimo se define un try donde intenta establecer la conexión a la base de datos y luego inicia el servidor en el puerto especificado en la variable de entorno PORT de lo contrario el catch nos mostraria error por consola.

 ## userControllers.js : 
  Se importa del archivo userSchema.js el objeto a trabajar y con base en el se elaboran las siguientes funciones.
 Función asincrona createUser : tiene por parametro un request como solicitud de un objeto, se crea un nuevo usuario en la base de datos, por condicion (!) se le pasa un return con el mensaje de falta completar datos como validacion. Luego se instancia UserSchema con los datos recibidos en la base de datos. El await esta esperando que se guarde, o no, para saber si muestra mensaje 200 'sucess' moatraria el usuario como respuesta o 401 error al cargar usuario(Crud)

Función getAllUsers : similar y tambien asincronica como createUser pero esta usa UserSchema.find() para devolver todos los usuarios, mediante un try catch. (cRud)

Función updateUsers : se encarga de actualizar un usuario existente en la base de datos mediante $set se le indica a mongo que debe actualizar y apunta al request del objeto . (crUd)

Función deleteUserByID  : se utiliza para eliminar un usuario de la base de datos. Recibe un identificador de usuario como parámetro en la URL (cruD)

## UserRoutes.js 
El codigo configura un enrutador de Express que define las rutas relacionadas con las operaciones de los usuarios. Este importalas funciones que vienen del archivo userControllers.

### Instalacion 
desde directorio apuntando a carpeta backEnd
 
  npm i  

  npm run dev
-------


# frontEnd


## dependencias
    "autoprefixer": "^10.4.14",
    "mongodb": "^5.7.0",
    "postcss": "^8.4.26",
    "react": "^18.2.0",
    "react-dom": "^18.2.0",
    "react-router-dom": "^6.14.1",
    "styled-components": "^6.0.4"

-------
## main.js 
 "createBrowserRouter" es una funcion que me permite definir un router (ruta) por medio de objetos, haciendo que <RouterProvider/> sea por donde comienzan a pasar los datos hacias otros componentes.
en si es un arreglo en el cual se le definen las rutas de la app mendiante objetos. se le define ruta (path), element lo que se renderizariaen pantalla y children que heredaria lo de element.
Se elaboran 2 carpetas "componentes" contiene lo reutilizable y "pages" lo necesario de nuestra app.
Se opto usar styled-components para embeber css dentro del mismo archivo .jsx.
solo de dejo un index.css de manera general.





### Instalacion 

desde directorio apuntando a carpeta frontEnd

 npm i  

 npm run dev
-------
